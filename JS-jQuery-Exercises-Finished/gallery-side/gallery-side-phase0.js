$(function() {

    var imgViewer = $('.img-viewer')
    var bigImg = imgViewer.find('img')
    var thumbnails = $('.thumbnails a')

    function showImage(event) {
        var currentImgSrc = bigImg.attr('src')
        var newImgSrc = $(this).attr('href')

        event.preventDefault()

        if (currentImgSrc === newImgSrc) {
            return
        }

        bigImg.attr({
            src: newImgSrc
        })
    }


    thumbnails.on('click', showImage)

})
