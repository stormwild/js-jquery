# Apply Javascript

## Learning Objectives

- To apply javascript inline
- To apply javascript embedded within a page 
- To link to an external javascript file

## Demo

Show examples of each

- Inline
- Embedded
- External

## Exercise

- Add an inline script to the body tag.
- Add an inline script to a link.
- Add an embedded script tag to the head.
- Add an embedded script tag to the body.
- Add an external javascript file

## Notes

JavaScript blocks browser page rendering.

JavaScript executes as it is encountered within the HTML page.

Use the browser console to display information. 

The console is useful for debugging.

Most web browsers keep track of JavaScript errors and record them in a separate window called an error console. When you load a web page that contains an error, you can then view the console to get helpful information about the error, like which line of the web page it occurred in and a description of the error.

JavaScript is executed in response to events to which they are assigned such as onload and onclick.

## Advanced

Use async or defer to prevent embedded scripts from blocking page rendering

However order of when scripts execute cannot be determined 
 