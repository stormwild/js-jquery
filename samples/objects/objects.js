var o = new Object();
n = { 
    firstName: 'Nancy', 
    lastName: 'Smith'
};

o.firstName = "John";
o.lastName = "Smith";

console.log(o);

var peter = {
    firstName: "Peter",
    lastName: "Smith",
    getFullName: function() {
        return this.firstName + ' ' + this.lastName
    },
    greetMe: function(msg) {
        return msg  + ' ' + this.firstName;
    }
}

console.log( peter.getFullName() );

// functions and objects are assigned/passed by reference
// function are also objects
// functions have built in function call apply
// this refers to the executing context
console.log( peter.getFullName.call(o) );
console.log( peter.getFullName.apply(o) );

console.log( peter.greetMe('Hi') );
console.log( peter.greetMe.call(o, 'Hi') ); // accepts
console.log( peter.greetMe.apply(o, ['Hi']) ); // accepts an array of params

console.log(window.o.firstName);
console.log(window.n);
console.log($);
var $ = {};
console.log($, jQuery);