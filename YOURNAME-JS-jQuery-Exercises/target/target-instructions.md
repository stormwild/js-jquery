# Target

## Phase 0
1. Declare a function named showBox. Keep this function empty for now.
2. Attach a click event handler to the element with an ID of appear-button,
   passing in the showBox function from #1.
3. Add functionality to the showBox function.
   a. Show the element with an ID of appear with one of the "showing" built-in
      effects.

## Phase 1
4. Declare a function named hideBox. Keep this function empty for now.
5. Attach a click event handler to the element with an ID of disappear-button,
   passing in the hideBox function from #4.
6. Add functionality to the hideBox function.
   a. Hide the element with an ID of disappear with one of the "hiding" built-in
      effects.

## Phase 2
7. Declare a function named toggleBox. Keep this function empty for now.
8. Attach a click event handler to the element with an ID of toggle-button,
   passing in the toggleBox function from #7.
9. Add functionality to the toggleBox function.
   a. Show the element with an ID of toggle with one of the "toggling" built-in
      effects.

## Phase 3
10. Modify the speed and easing of the built-in effects.


## Phase 4
11. Open target-single.html in your browser.
12. Declare a function named animateBox. Keep this function empty for now.
13. Attach a click event handler to the element with an ID of animate-button,
    passing in the animateBox function from #12.
14. Add functionality to the animateBox function.
    a. Animate the element with an ID of animate using the jQuery animate
       method.
