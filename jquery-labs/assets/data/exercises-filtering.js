var jQLabsData = jQLabsData || {};
jQLabsData["exercises-filtering"] = {
  "exercises": [
    {
      "id": 1,
      "category": "filtering",
      "instructions": "<h2>Remove all article elements from the collection except the 1st one</h2>\n",
      "htmlStart": "<section class=\"tabs\">\n  <nav class=\"tabs-nav\">\n    <a href=\"#\" class=\"current\">First</a>\n    <a href=\"#\">Second</a>\n    <a href=\"#\">Third</a>\n    <a href=\"#\">Fourth</a>\n  </nav>\n\n  <article data-correct=\"true\">\n    <h1>First</h1>\n    <p>Vivamus dictum orci ut pulvinar vehicula. Maecenas ultricies sodales nisl ut ornare.</p>\n  </article>\n  <article>\n    <h1>Second</h1>\n    <p>Duis condimentum accumsan metus sit amet malesuada. Donec non elit massa.</p>\n  </article>\n  <article>\n    <h1>Third</h1>\n    <p>Nam venenatis vel tellus in iaculis. Donec mollis, massa ut imperdiet semper.</p>\n  </article>\n  <article>\n    <h1>Fourth</h1>\n    <p>Augue nisl euismod ligula, sit amet mollis sem eros nec purus. Maecenas vel risus mi.</p>\n  </article>\n</section>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s=",
        "preSelector": "$('.tabs article')"
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 2,
      "category": "filtering",
      "instructions": "<h2>Filter the collection so that only the 5th paragraph remains</h2>\n",
      "htmlStart": "<section class=\"container\">\n  <p>Labore quis anim tempor veniam Lorem cillum.</p>\n\n  <p>Irure magna eu laboris do nostrud officia voluptate.</p>\n\n  <p>Exercitation ullamco eu adipisicing sunt eiusmod. Fugiat minim.</p>\n\n  <p>Excepteur magna eiusmod enim Lorem esse sit quis est quis.</p>\n\n  <p data-correct=\"true\">Magna officia mollit amet eiusmod deserunt Lorem pariatur est magna culpa nisi exercitation ut.</p>\n\n  <a href=\"#\">Commodo non occaecat minim enim cupidatat.</a>\n</section>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s=",
        "preSelector": "$('.container p')"
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 3,
      "category": "filtering",
      "instructions": "<h2>Filter the collection so that only the 3rd article remains</h2>\n",
      "htmlStart": "<section class=\"tabs\">\n  <nav class=\"tabs-nav\">\n    <a href=\"#\" class=\"current\">First</a>\n    <a href=\"#\">Second</a>\n    <a href=\"#\">Third</a>\n    <a href=\"#\">Fourth</a>\n  </nav>\n\n  <article>\n    <h1>First</h1>\n    <p>Vivamus dictum orci ut pulvinar vehicula. Maecenas ultricies sodales nisl ut ornare.</p>\n  </article>\n  <article>\n    <h1>Second</h1>\n    <p>Duis condimentum accumsan metus sit amet malesuada. Donec non elit massa.</p>\n  </article>\n  <article data-correct=\"true\">\n    <h1>Third</h1>\n    <p>Nam venenatis vel tellus in iaculis. Donec mollis, massa ut imperdiet semper.</p>\n  </article>\n  <article>\n    <h1>Fourth</h1>\n    <p>Augue nisl euismod ligula, sit amet mollis sem eros nec purus. Maecenas vel risus mi.</p>\n  </article>\n</section>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s=",
        "preSelector": "$('.tabs article')"
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 4,
      "category": "filtering",
      "instructions": "<h2>Filter the collection so that only the <code>&lt;div&gt;</code> with a text of &quot;Target&quot; remains</h2>\n",
      "htmlStart": "<div>\n  <div>Nostrud eu fugiat elit veniam</div>\n\n  <div>Duis lorem in excepteur enim</div>\n\n  <section>\n    <div>Donec eleifend dapibus semper.</div>\n    <div>Lorem esse nulla exercitation in</div>\n  </section>\n\n  <div data-correct=\"true\"><strong>Target</strong></div>\n\n  <div>Nulla et cupidatat laboris do</div>\n</div>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s=",
        "preSelector": "$('div > div')"
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 5,
      "category": "filtering",
      "instructions": "<h2>Filter the collection so that only the anchor with a &quot;<code>current</code>&quot; class remains</h2>\n",
      "htmlStart": "<section class=\"accordion\">\n  <a href=\"#\">First</a>\n  <article>\n    <p>Mollit ullamco sint sunt exercitation aliquip nostrud ullamco nisi voluptate voluptate irure do voluptate.</p>\n  </article>\n\n  <a data-correct=\"true\" href=\"#\" class=\"current\">Second</a>\n  <article>\n    <p>Lorem enim est dolore deserunt dolore cillum exercitation.</p>\n  </article>\n\n  <a href=\"#\">Third</a>\n  <article>\n    <p>Eiusmod occaecat aute sint Lorem aute aliqua tempor.</p>\n  </article>\n\n  <a href=\"#\">Fourth</a>\n  <article>\n    <p>Laborum dolor aute labore sunt ut in voluptate cupidatat mollit mollit ex ex ea ad.</p>\n  </article>\n</section>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s=",
        "preSelector": "$('.accordion a')"
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 6,
      "category": "filtering",
      "instructions": "<h2>Remove the paragraphs from the collection that have a &quot;<code>peanut</code>&quot; class</h2>\n",
      "htmlStart": "<article>\n  <p class=\"peanut\">Quis mollis ipsum bibendum. Sed tristique eget eros sit amet.</p>\n  <p data-correct=\"true\">Bibendum integer at erat faucibus neque sodales.</p>\n  <p class=\"peanut\">A leo tempus molestie. Suspendisse lacinia mattis erat et.</p>\n  <p data-correct=\"true\">Elementum et sed justo. In vitae elit.</p>\n  <p data-correct=\"true\">Mollis fusce ultrices convallis turpis, et aliquet sapien.</p>\n  <p class=\"peanut\">Imperdiet non ut ac commodo.</p>\n</article>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s=",
        "preSelector": "$('article p')"
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 7,
      "category": "filtering",
      "instructions": "<h2>Remove the first article element from the collection</h2>\n",
      "htmlStart": "<div>\n  <a href=\"#\">First</a>\n  <article>\n    <p>Mollit ullamco sint sunt exercitation aliquip nostrud ullamco nisi voluptate voluptate irure do voluptate.</p>\n  </article>\n\n  <a href=\"#\" class=\"current\">Second</a>\n  <article data-correct=\"true\">\n    <p>Lorem enim est dolore deserunt dolore cillum exercitation.</p>\n  </article>\n\n  <a href=\"#\">Third</a>\n  <article data-correct=\"true\">\n    <p>Eiusmod occaecat aute sint Lorem aute aliqua tempor.</p>\n  </article>\n\n  <a href=\"#\">Fourth</a>\n  <article data-correct=\"true\">\n    <p>Laborum dolor aute labore sunt ut in voluptate cupidatat mollit mollit ex ex ea ad.</p>\n  </article>\n</div>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s=",
        "preSelector": "$('div article')"
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 8,
      "category": "filtering",
      "instructions": "<h2>Remove the even table rows from the collection</h2>\n",
      "htmlStart": "<table>\n  <thead>\n    <tr>\n      <th>First Name</th>\n      <th>Price</th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr>\n      <td>Tanya</td>\n      <td>217</td>\n    </tr>\n    <tr data-correct=\"true\">\n      <td>Patrick</td>\n      <td>240</td>\n    </tr>\n    <tr>\n      <td>Lauren</td>\n      <td>180</td>\n    </tr>\n    <tr data-correct=\"true\">\n      <td>Terrence</td>\n      <td>169</td>\n    </tr>\n    <tr>\n      <td>Melissa</td>\n      <td>150</td>\n    </tr>\n  </tbody>\n</table>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s=",
        "preSelector": "$('tbody tr')"
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 9,
      "category": "filtering",
      "instructions": "<h2>Filter the collection so that only the <code>h2</code>s and paragraphs remain</h2>\n",
      "htmlStart": "<div>\n  <h2 data-correct=\"true\">Lorem</h2>\n  <p data-correct=\"true\">Mollit ullamco sint sunt exercitation aliquip nostrud ullamco nisi voluptate voluptate irure do voluptate.</p>\n  <h2 data-correct=\"true\">Ipsum</h2>\n  <p data-correct=\"true\">Lorem enim est dolore deserunt dolore cillum exercitation.</p>\n  <a href=\"#\"><h4>Dolor</h4></a>\n  <div>Eiusmod occaecat aute sint Lorem aute aliqua tempor.</div>\n  <h2 data-correct=\"true\">Sit</h2>\n  <p data-correct=\"true\">Laborum dolor aute labore sunt ut in voluptate cupidatat mollit mollit ex ex ea ad.</p>\n  <h3>Amet</h3>\n</div>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s=",
        "preSelector": "$('h2, h3, p, a')"
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    }
  ]
};
