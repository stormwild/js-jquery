# Accordion

## Phase 0
1. Select all anchor elements that are descendants of ".accordion". Store them
   in a variable (headings).
2. Declare a function named showPanel. Keep this function empty for now.
3. Hide all headings' next siblings except for the first one.
4. Attach a click event handler to headings, passing in the showPanel function
   from #2.
5. Add functionality to the showPanel function.
   a. Store the jQuery-wrapped "this" in a variable (heading).
   b. Prevent the click event's default behavior.
   c. Get heading's next sibling and slide it down.
   d. Continuing the method chain from 5.c., get the active element(s)' article
      element siblings and slide them up.


## Phase 1
6. Add more functionality to the showPanel function.
   a. Give the element in the heading variable a class of "current".
   b. Remove the "current" class from heading's anchor element siblings.
