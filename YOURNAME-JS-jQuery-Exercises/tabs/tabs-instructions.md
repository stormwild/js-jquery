# Tabs

## Phase 0
1. Select all anchor elements that are descendants of ".tabs-nav". Store them in
   a variable (tabLinks).
2. Select all article elements that are descendants of ".tabs. Store them in a
   variable (tabPanels).
3. Declare a function named showPanel. Keep this function empty for now.
4. Hide all elements in the tabPanels variable except for the first one.
5. Attach a click event handler to tabLinks, passing in the showPanel function
   from #3.
6. Add functionality to the showPanel function.
   a. Pass the anchor that was clicked to the jQuery function. Store it in a
      variable (tab).
   b. Get the index of the tab variable. Store it in a variable (index).
   c. Prevent the click event's default behavior.
   d. Hide the elements in the tabPanels variable.
   e. Get the element in tabPanels with the index equal to the index
      variable in 6.b and show it.


## Phase 1
7. Add more functionality to the showPanel function.
   a. Give the element in the tab variable a class of "current".
   b. Remove the "current" class from tab's sibling elements.
