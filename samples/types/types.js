var customer = "John \"Joe\" Smith";

console.log(typeof "John \"Joe\" Smith");
console.log(typeof customer);

console.log( customer.length );
console.log( customer.substr(6, 3) );
console.log( customer.indexOf('J') );
console.log( customer.toLowerCase() );
console.log( customer.toUpperCase() );
console.log( customer.replace(/J/g, 'L') );
console.log( customer + ' is ' + 25 + ' years old.' );

var link = 'http://www.example.com';
var fruits = "apple, orange, melon";

console.log(fruits.split(','));


