(function(window, $) {

    var $buttons = $('button')

    $buttons.each(function(i, button) {
        var fnName = button.getAttribute('data-fn-name')
        var fn = window[fnName]

        if (typeof fn != 'function') {
            button.innerHTML = fnName + ' function not found'
            button.className += ' fn-not-found'
            return
        }

        $.data(button, 'fn', fn)
    })

    function executeAssociatedFn(event) {
        var fn = $.data(this, 'fn')
        fn && fn()
    }

    $buttons.on('click', executeAssociatedFn);

}(window, jQuery))
