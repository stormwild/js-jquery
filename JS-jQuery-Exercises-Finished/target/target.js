$(function() {

    function showBox() {
        // $('#appear').show()
        // $('#appear').fadeIn()
        $('#appear').slideDown(800, 'easeOutBounce')
    }

    function hideBox() {
        // $('#disappear').hide()
        $('#disappear').fadeOut()
        // $('#disappear').slideUp(800, 'easeInElastic')
    }

    function toggleBox() {
        $('#toggle').toggle()
        // $('#toggle').fadeToggle()
        // $('#toggle').slideToggle(800, 'easeInOutBounce')
    }

    function animateBox() {
        $('#animate').animate({
            left: 600,
            width: 100,
            height: 100,
            opacity: 0.3
        }, 600, 'easeOutBounce')
    }


    $('#appear-button').on('click', showBox)
    $('#disappear-button').on('click', hideBox)
    $('#toggle-button').on('click', toggleBox)
    $('#animate-button').on('click', animateBox)

})
