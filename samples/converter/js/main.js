(function (window, undefined){
    
    console.log(typeof window);
    console.log(typeof undefined);
    
    var $form = document.getElementById('converter'),
        $amount = document.getElementById('amount'),
        $result = document.getElementById('result'),
        rate = 45.0;
    
    $form.addEventListener('submit', function(e){
        e.preventDefault();
        
        var result = ($amount.value / rate).toFixed(2);
        
        $result.innerHTML = 'Result: ' + result + ' in USD';
    });
    
    
    
})(window);