(function ($) {

    var output = '',
        $books = $('.books'),
        $bookTmpl = $('#book-tmpl');
var mydata;
    function displayBooks(data) {
        $.each(data, function (i, v) {
            output += $bookTmpl.html()
                .replace(/{{ title }}/, v.title)
                .replace(/{{ author }}/, v.author)
        }); 
        $books.html(output);
    }
    
    function errorHandler(jqxhr, status, error) {               console.log(status, error);
    }
    
    $.ajax('data/books.json', {
        type: 'get',
        dataType: 'json',
        data: {
            page: 4,
            total: 10,
        }
    })
        .done(displayBooks)
        .fail(errorHandler)
        .always();
    
    
})(jQuery);

