var zaphod = {name: 'Zaphod', age: 42};
var marvin = {name: 'Marvin', age: 420000000000};
var name = 'Jane';

zaphod.sayHello = function(){
  return "Hi, I'm " + this.name;
}

var sayHello = zaphod.sayHello;

marvin.sayHello = zaphod.sayHello;

//console.log( zaphod.sayHello() );

//console.log( marvin.sayHello() );

//console.log( sayHello() ); // 

// functions

function add() {
    var total = 0;
    var len = arguments.length;
    for(i = 0; i < len; i++) {
        total += arguments[i];
    }
    return total;
}

console.log( add(2, 2, 16, 2, 5, 10) );