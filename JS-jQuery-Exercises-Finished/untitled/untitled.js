function styleSingleBox() {
    $('.css-0-target').css({
        'width': 100,
        'height': 100,
        'border-radius': '50%',
        'transition': 'all 0.4s',
        'transform': 'rotate(180deg)'
    })
}

function styleMultipleBoxes() {
    $('.css-1-target').css({
        'transition': 'all 0.2s',
        'border-radius': '0px 50%',
        'background-color': '#abcdef'
    })
}

function addClasses() {
    $('.add-class-target').addClass('untitled')
}

function removeClasses() {
    $('.remove-this').removeClass('remove-this')
}

function toggleClasses() {
    $('.toggle-class-target').toggleClass('untitled')
}
