var greeting = "Hi";

function sayHi(name) {
    return this.greeting + ' ' + name;
}

var person = {
    greeting: 'Hello',
    sayHi: sayHi
};

console.log( sayHi('Peter') );

console.log( person.sayHi('John') );



