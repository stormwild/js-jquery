$(function() {

    var nav = $('.main-nav')
    var win = $(window)
    var navTop = nav.offset().top

    function toggleStickyNav() {
        if (win.scrollTop() > navTop) {
            nav.addClass('sticky')
        } else {
            nav.removeClass('sticky')
        }
    }


    win.on('scroll', toggleStickyNav)
    toggleStickyNav()

});
