(function(window, undefined){

    var $form = document.getElementById('converter'),
        $amountField = document.getElementById('amount'),
        $result = document.getElementById('result'),
        rate = 45;
    
    $form.addEventListener('submit', function(e){
        e.preventDefault();
        
        $result.innerHTML = (+$amountField.value / rate).toFixed(2);
    });
    
    var el = document.querySelectorAll('form input');
    
})(window);