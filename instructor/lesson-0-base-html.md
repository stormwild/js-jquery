# Base HTML

## Learning Objectives

- To create a minimal html page

## Demo

Show base html code.

Review and discuss basic terms

## Exercise

- Create a bare bones html page
- Link to a css file

## Notes

- HTML Elements consist of a start or opening tag and an end or closing tag
- Elements have attributes: id, class, title, style, events such as onload, onclick, onmouseover 
- Attributes have an attribute name and usually a value surrounded by quotes
- CSS review  


 