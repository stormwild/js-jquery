$(function() {

    var headings = $('.accordion a')

    function showPanel(event) {
        var heading = $(this)

        event.preventDefault()

        heading
            .next()
            .slideDown(200)
            .siblings('article')
            .slideUp(200)
    }


    headings
        .next()
        .not(':first')
        .hide()

    headings.on('click', showPanel)

})
