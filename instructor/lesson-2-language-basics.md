# Language Basics

ECMAScript as defined in ECMA-262, third edition, is the most-implemented version among web browsers. The fifth edition is the next to be implemented in browsers, though, as of the end of 2011, no browser has fully implemented it.

Note: Check status

## Case Sensitivity

Everything is case sensitive: variables, function names, and operators are all case-sensitive.

test is different from Test and typeof a keyword, is different from typeOf

## Identifiers

Identifiers may be one or more characters in the following format:

* The first character must be letter, underscore, or a dollar sign.
* All other characters may be letters, underscores, dollar signs or numbers.

By convention identifiers use camel case, meaning the first letter is lowercase and each additional word is offset by a capital letter such as:

firstSecond
myCar
doSomethingImportant

Keywords like typeof, true, false and null among others cannot be used as identifiers.

## Comments

```js
// single line comment

/*
Multiline
comment
*/

/*
 * Prefixing a line with an asterisk can aid readability
 */
```

## Strict Mode

ECMAScript 5 introduced the concept of strict mode.

Strict mode is a different parsing and execution model for Javscript.

To enable strict mode, include the following at the top:

```js
"use strict";
```

You can also specify just a function to execute in script mode by including it at the top of the function body:

```js
function doAction() {
    "use strict";
    // function body
}
```

IE 10, Firefox 4+, Safari 5.1+, Opera 12+ and Chrome support strict mode.

## Statements

Statements are terminated by a semi-colon, omitting the semi-colon makes the parser determine where the end of the statement occurs.

Although not require it is best practice to use semi-colons at the end of each statement.



