var jQLabsData = jQLabsData || {};
jQLabsData["exercises-selecting"] = {
  "exercises": [
    {
      "id": 1,
      "category": "selecting",
      "instructions": "<h2>Select all paragraph elements</h2>\n",
      "htmlStart": "<h3>Lorem ipsum dolor sit amet</h3>\n\n<p data-correct=\"true\">Maecenas bibendum augue sed est sagittis iaculis et at ligula.</p>\n\n<ul>\n  <li>\n    <p data-correct=\"true\">Id ut nisl. Integer lobortis dui molestie urna euismod semper.</p>\n  </li>\n  <li>Mauris cursus lacinia</li>\n</ul>\n\n<p data-correct=\"true\">Suspendisse interdum orci diam, in euismod mi consequat at.</p>\n\n<ul>\n  <li>\n    <p data-correct=\"true\">Mauris vitae facilisis erat. Mauris at magna in diam euismod scelerisque.</p>\n  </li>\n  <li>Ut ligula.</li>\n</ul>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s="
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 2,
      "category": "selecting",
      "instructions": "<h2>Select all list items</h2>\n",
      "htmlStart": "<ol>\n  <li data-correct=\"true\">Loren</li>\n  <li data-correct=\"true\">Edson</li>\n  <li data-correct=\"true\">Dolores</li>\n</ol>\n\n<ul>\n  <li data-correct=\"true\">Sid</li>\n  <li data-correct=\"true\">Ahmed</li>\n  <li data-correct=\"true\">Bibingka</li>\n</ul>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s="
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 3,
      "category": "selecting",
      "instructions": "<h2>Select the element with an ID of &quot;<code>me</code>&quot;</h2>\n<!-- Prefixing your ID selector with the element's tag name is, in most cases, -->\n<!-- unnecessary. -->\n\n<!-- **Note:** ID selectors should only ever return zero or one element. -->\n",
      "htmlStart": "<div>Vestibulum bibendum, justo non volutpat fermentum.</div>\n<div>Dui risus fermentum tellus, ac ultrices turpis dolor non arcu.</div>\n<div>Donec vehicula nisl tortor.</div>\n<div data-correct=\"true\" id=\"me\"><strong>Select me!</strong></div>\n<div>Quisque elementum et ipsum id ullamcorper.</div>\n<div>Sed fermentum, urna vel tempus volutpat, est erat.</div>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s="
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 4,
      "category": "selecting",
      "instructions": "<h2>Select the element with a class of &quot;<code>main-nav</code>&quot;</h2>\n<!-- Prefixing class selectors with the tag name is sometimes unnecessary. In this -->\n<!-- case only the unordered list (`<ul>`) has a class of \"`main-nav`\" so it's okay to -->\n<!-- leave the tag name out. -->\n",
      "htmlStart": "<ul data-correct=\"true\" class=\"main-nav\">\n  <li><a href=\"#\">Home</a></li>\n  <li><a href=\"#\">Gallery</a></li>\n  <li><a href=\"#\">About</a>\n    <ul>\n      <li><a href=\"#\">Loren</a></li>\n      <li><a href=\"#\">Epson</a></li>\n      <li><a href=\"#\">Dolores</a></li>\n    </ul>\n  </li>\n  <li><a href=\"#\">Contact</a></li>\n  <li><a href=\"#\">FAQ</a></li>\n</ul>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s="
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 5,
      "category": "selecting",
      "instructions": "<h2>Select the unordered list with a class of &quot;<code>main-nav</code>&quot;</h2>\n<!-- Here is a contrived case where qualifying a class selector with the element's -->\n<!-- tag name is important. You want to select only the unordered list (`<ul>`) -->\n<!-- with the \"`main-nav`\" class and not the nav (`<nav>`) element. -->\n",
      "htmlStart": "<nav class=\"main-nav\">\n  <a href=\"#\">Home</a>\n  <a href=\"#\">Gallery</a>\n  <a href=\"#\">About</a>\n</nav>\n\n<ul data-correct=\"true\" class=\"main-nav\">\n  <li><a href=\"#\">Home</a></li>\n  <li><a href=\"#\">Gallery</a></li>\n  <li><a href=\"#\">About</a>\n    <ul>\n      <li><a href=\"#\">Loren</a></li>\n      <li><a href=\"#\">Epson</a></li>\n      <li><a href=\"#\">Dolores</a></li>\n    </ul>\n  </li>\n  <li><a href=\"#\">Contact</a></li>\n  <li><a href=\"#\">FAQ</a></li>\n</ul>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s="
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 6,
      "category": "selecting",
      "instructions": "<h2>Select all paragraphs with a class of &quot;<code>eanut</code>&quot;</h2>\n<!-- Here is a case where qualifying a class selector with the element's tag name is -->\n<!-- important. You want to select only the paragraphs (`<p>`) that have the -->\n<!-- \"`eanut`\" class and not the paragraphs without it nor the div (`<div>`) with -->\n<!-- it. -->\n",
      "htmlStart": "<p data-correct=\"true\" class=\"eanut\">Sed at facilisis mauris, id hendrerit diam. Nam sagittis lorem id.</p>\n\n<p><strong>It's a trap!</strong></p>\n\n<p data-correct=\"true\" class=\"eanut\">Tellus consequat pulvinar. Praesent ipsum sem, tincidunt quis purus sed, dapibus.</p>\n\n<p><strong>It's a trap!</strong></p>\n\n<div class=\"eanut\"><strong>It's a trap!</strong></div>\n\n<p data-correct=\"true\" class=\"eanut\">Commodo magna. Sed feugiat ante lorem, quis bibendum felis fermentum et.</p>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s="
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 7,
      "category": "selecting",
      "instructions": "<h2>Select all <code>input</code> and <code>textarea</code> elements</h2>\n",
      "htmlStart": "<form>\n\n  <label for=\"name\">Name</label>\n  <input data-correct=\"true\" type=\"text\" id=\"name\">\n\n  <label for=\"email\">Email address</label>\n  <input data-correct=\"true\" type=\"email\" id=\"email\">\n\n  <label for=\"message\">Message</label>\n  <textarea data-correct=\"true\" id=\"message\"></textarea>\n\n  <label for=\"spam\">Yes, I want spam!</label>\n  <input data-correct=\"true\" id=\"spam\" type=\"checkbox\">\n\n</form>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s="
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 8,
      "category": "selecting",
      "instructions": "<h2>Select all anchors that are descendants of <code>.accordion</code></h2>\n",
      "htmlStart": "<section class=\"accordion\">\n  <a data-correct=\"true\" href=\"#\" class=\"current\">First</a>\n  <article>\n    <p>Mollit ullamco sint sunt exercitation aliquip nostrud ullamco nisi voluptate voluptate irure do voluptate.</p>\n  </article>\n\n  <a data-correct=\"true\" href=\"#\">Second</a>\n  <article>\n    <p>Lorem enim est dolore deserunt dolore cillum exercitation.</p>\n  </article>\n\n  <a data-correct=\"true\" href=\"#\">Third</a>\n  <article>\n    <p>Eiusmod occaecat aute sint Lorem aute aliqua tempor.</p>\n  </article>\n\n  <a data-correct=\"true\" href=\"#\">Fourth</a>\n  <article>\n    <p>Laborum dolor aute labore sunt ut in voluptate cupidatat mollit mollit ex ex ea ad.</p>\n  </article>\n</section>\n\n<section>\n  <a href=\"#\"><strong>Trap</strong>th</a>\n</section>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s="
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 9,
      "category": "selecting",
      "instructions": "<h2>Select all anchors that are children of <code>.accordion</code></h2>\n",
      "htmlStart": "<section class=\"accordion\">\n  <a data-correct=\"true\" href=\"#\" class=\"current\">First</a>\n  <article>\n    <p>Mollit <a href=\"#trap\">ullamco sint</a> sunt exercitation aliquip nostrud ullamco nisi voluptate voluptate irure do voluptate.</p>\n  </article>\n\n  <a data-correct=\"true\" href=\"#\">Second</a>\n  <article>\n    <p>Lorem enim est dolore deserunt dolore cillum exercitation.</p>\n  </article>\n\n  <a data-correct=\"true\" href=\"#\">Third</a>\n  <article>\n    <p>Eiusmod occaecat aute sint Lorem aute aliqua tempor.</p>\n  </article>\n\n  <a data-correct=\"true\" href=\"#\">Fourth</a>\n  <article>\n    <p>Laborum dolor aute labore <a href=\"#trap\">sunt ut in voluptate</a> cupidatat mollit mollit ex ex ea ad.</p>\n  </article>\n</section>\n\n<section>\n  <a href=\"#\"><strong>Trap</strong>th</a>\n</section>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s="
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 10,
      "category": "selecting",
      "instructions": "<h2>Select all divs that are children of a div</h2>\n",
      "htmlStart": "<div>\n\n  <div data-correct=\"true\">Nostrud eu fugiat elit veniam</div>\n\n  <div data-correct=\"true\">Duis lorem in excepteur enim</div>\n\n  <div data-correct=\"true\">Lorem esse nulla exercitation in</div>\n\n  <div data-correct=\"true\">Nulla et cupidatat laboris do</div>\n\n  <section>\n    <div>It's a trap!</div>\n  </section>\n\n</div>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s="
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 11,
      "category": "selecting",
      "instructions": "<h2>Select all <code>input</code> elements of type text</h2>\n",
      "htmlStart": "<form>\n  <label for=\"first-name\">First name</label>\n  <input data-correct=\"true\" id=\"first-name\" type=\"text\" placeholder=\"First name\">\n\n  <label for=\"last-name\">Last name</label>\n  <input data-correct=\"true\" id=\"last-name\" type=\"text\" placeholder=\"Last name\">\n\n  <label for=\"password\">Password</label>\n  <input id=\"password\" type=\"password\">\n\n  <label for=\"repeat-password\">Repeat password</label>\n  <input id=\"repeat-password\" type=\"password\">\n</form>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s="
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 12,
      "category": "selecting",
      "instructions": "<h2>Select all radio and checkbox inputs</h2>\n",
      "htmlStart": "<form>\n  <label for=\"character-name\">Character name</label>\n  <input id=\"character-name\" type=\"text\" placeholder=\"Character name\">\n\n  <label for=\"lawful\">Lawful</label>\n  <input data-correct=\"true\" id=\"lawful\" type=\"radio\" name=\"alignment\" value=\"lawful\">\n\n  <label for=\"neutral\">Neutral</label>\n  <input data-correct=\"true\" id=\"neutral\" type=\"radio\" name=\"alignment\" value=\"neutral\">\n\n  <label for=\"chaotic\">Chaotic</label>\n  <input data-correct=\"true\" id=\"chaotic\" type=\"radio\" name=\"alignment\" value=\"chaotic\">\n\n  <label for=\"hardcore-mode\">Hardcore mode</label>\n  <input data-correct=\"true\" id=\"hardcore-mode\" type=\"checkbox\">\n\n  <input type=\"button\" value=\"Start\">\n</form>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s="
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    }
  ]
};
