$(function() {

    function stickyNav(nav) {
        var win = $(window)
        var navTop = nav.offset().top

        function toggleStickyNav() {
            if (win.scrollTop() > navTop) {
                nav.addClass('sticky')
            } else {
                nav.removeClass('sticky')
            }
        }

        win.on('scroll', toggleStickyNav)
        toggleStickyNav()
    }

    function smoothScroll(linkContainer) {
        var jumpLinks = linkContainer.find('a')
        var scrollables = $('html, body')
        var navHeight = linkContainer.height()

        function doScroll(event) {
            var id = $(this).attr('href')
            var top = $(id).offset().top

            event.preventDefault()

            scrollables.animate({
                scrollTop: top - navHeight
            })
        }

        jumpLinks.on('click', doScroll)
    }


    var mainNav = $('.main-nav')
    stickyNav(mainNav)
    smoothScroll(mainNav)

});
