(function($) {

    // Naive $.fn.attr duck punch to fake image loading latency.

    'use strict';

    var attr = $.fn.attr;

    $.fn.attr = function() {
        var self = this;
        var args = arguments;

        if (args.length === 1 && typeof args[0] === 'string') {
            return attr.apply(this, args);
        }

        setTimeout(function() {
            attr.apply(self, args);
        }, 600);

        return this;
    };

}(jQuery));
