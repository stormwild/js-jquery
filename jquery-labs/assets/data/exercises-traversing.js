var jQLabsData = jQLabsData || {};
jQLabsData["exercises-traversing"] = {
  "exercises": [
    {
      "id": 1,
      "category": "traversing",
      "instructions": "<h2>Traverse to the article elements</h2>\n",
      "htmlStart": "<section class=\"accordion\">\n  <a href=\"#\">First</a>\n  <article data-correct=\"true\">\n    <p>Mollit ullamco sint sunt exercitation aliquip nostrud ullamco nisi voluptate voluptate irure do voluptate.</p>\n  </article>\n\n  <a href=\"#\">Second</a>\n  <article data-correct=\"true\">\n    <p>Lorem enim est dolore deserunt dolore cillum exercitation.</p>\n  </article>\n\n  <a href=\"#\">Third</a>\n  <article data-correct=\"true\">\n    <p>Eiusmod occaecat aute sint Lorem aute aliqua tempor.</p>\n  </article>\n\n  <a href=\"#\">Fourth</a>\n  <article data-correct=\"true\">\n    <p>Laborum dolor aute labore sunt ut in voluptate cupidatat mollit mollit ex ex ea ad.</p>\n  </article>\n</section>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s=",
        "preSelector": "$('.accordion a')"
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 2,
      "category": "traversing",
      "instructions": "<h2>Traverse to the headings</h2>\n",
      "htmlStart": "<article>\n  <h3 data-correct=\"true\">Tempor in nulla et cupidatat</h3>\n  <p>Laboris do id excepteur ullamco exercitation adipisicing laborum ad. In ullamco fugiat.</p>\n  <a href=\"#\"><em>read more</em></a>\n</article>\n\n<article>\n  <h3 data-correct=\"true\">Magna elit voluptate eiusmod</h3>\n  <p>Eiusmod sit consectetur cillum elit labore ea commodo laboris nostrud incididunt veniam pariatur.</p>\n  <a href=\"#\"><em>read more</em></a>\n</article>\n\n<article>\n  <h3 data-correct=\"true\">Pariatur quis veniam pariatur elit aute</h3>\n  <p>In elit in laborum pariatur exercitation cupidatat dolore. Minim ut eiusmod exercitation dolore ex voluptate.</p>\n  <a href=\"#\"><em>read more</em></a>\n</article>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s=",
        "preSelector": "$('article > p')"
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 3,
      "category": "traversing",
      "instructions": "<h2>Traverse to the 1st, 2nd, and 4th anchor elements</h2>\n",
      "htmlStart": "<nav class=\"tabs-nav\">\n  <a data-correct=\"true\" href=\"#\">First</a>\n  <a data-correct=\"true\" href=\"#\">Second</a>\n  <a href=\"#\" class=\"current\">Third</a>\n  <a data-correct=\"true\" href=\"#\">Fourth</a>\n</nav>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s=",
        "preSelector": "$('a.current')"
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 4,
      "category": "traversing",
      "instructions": "<h2>Traverse to the paragraphs</h2>\n",
      "htmlStart": "<ul class=\"gallery\">\n  <li>\n    <a href=\"#\"> {{thumbnail}} </a>\n    <p data-correct=\"true\">Nostrud eu fugiat elit veniam duis Lorem in excepteur enim lorem esse nulla.</p>\n  </li>\n\n  <li>\n    <a href=\"#\"> {{thumbnail}} </a>\n    <p data-correct=\"true\">Exercitation tempor in nulla et cupidatat laboris do id excepteur ullamco.</p>\n  </li>\n\n  <li>\n    <a href=\"#\"> {{thumbnail}} </a>\n    <p data-correct=\"true\">Adipisicing laborum ad. In ullamco fugiat magna elit. Voluptate eiusmod eiusmod.</p>\n  </li>\n</ul>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s=",
        "preSelector": "$('.gallery a')"
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 5,
      "category": "traversing",
      "instructions": "<h2>Traverse to the headings and paragraphs</h2>\n",
      "htmlStart": "<div class=\"container\">\n  <h2 data-correct=\"true\">Labore quis anim tempor</h2>\n  <p data-correct=\"true\">Do nostrud officia voluptate. Exercitation ullamco eu adipisicing sunt eiusmod.</p>\n\n  <h2 data-correct=\"true\">Veniam Lorem cillum</h2>\n  <p data-correct=\"true\">Fugiat minim excepteur magna eiusmod enim Lorem esse sit quis est quis magna. Officia mollit amet.</p>\n\n  <h2 data-correct=\"true\">Irure magna eu laboris</h2>\n  <p data-correct=\"true\">Eiusmod deserunt Lorem pariatur est magna culpa nisi exercitation utnon occaecat minim enim .</p>\n</div>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s=",
        "preSelector": "$('.container')"
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 6,
      "category": "traversing",
      "instructions": "<h2>Traverse to the list items</h2>\n",
      "htmlStart": "<ul class=\"gallery\">\n  <li data-correct=\"true\">\n    <a href=\"#\"> {{thumbnail}} </a>\n    <p>Nostrud eu fugiat elit veniam duis Lorem in excepteur enim lorem esse nulla.</p>\n  </li>\n\n  <li data-correct=\"true\">\n    <a href=\"#\"> {{thumbnail}} </a>\n    <p>Exercitation tempor in nulla et cupidatat laboris do id excepteur ullamco.</p>\n  </li>\n\n  <li data-correct=\"true\">\n    <a href=\"#\"> {{thumbnail}} </a>\n    <p>Adipisicing laborum ad. In ullamco fugiat magna elit. Voluptate eiusmod eiusmod.</p>\n  </li>\n</ul>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s=",
        "preSelector": "$('.gallery a')"
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 7,
      "category": "traversing",
      "instructions": "<h2>Traverse to the 1st, 3rd, and 4th anchor elements</h2>\n",
      "htmlStart": "<section class=\"accordion\">\n  <a data-correct=\"true\" href=\"#\">First</a>\n  <article>\n    <p>Mollit ullamco sint sunt exercitation aliquip nostrud ullamco nisi voluptate voluptate irure do voluptate.</p>\n  </article>\n\n  <a href=\"#\" class=\"current\">Second</a>\n  <article>\n    <p>Lorem enim est dolore deserunt dolore cillum exercitation.</p>\n  </article>\n\n  <a data-correct=\"true\" href=\"#\">Third</a>\n  <article>\n    <p>Eiusmod occaecat aute sint Lorem aute aliqua tempor.</p>\n  </article>\n\n  <a data-correct=\"true\" href=\"#\">Fourth</a>\n  <article>\n    <p>Laborum dolor aute labore sunt ut in voluptate cupidatat mollit mollit ex ex ea ad.</p>\n  </article>\n</section>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s=",
        "preSelector": "$('a.current')"
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 8,
      "category": "traversing",
      "instructions": "<h2>Traverse to the list item with a class of target</h2>\n",
      "htmlStart": "<ul class=\"gallery\">\n  <li>\n    <a href=\"#\"> {{thumbnail}} </a>\n    <p>Nostrud eu fugiat elit veniam duis Lorem in excepteur enim lorem esse nulla.</p>\n  </li>\n\n  <li data-correct=\"true\" class=\"target\">\n    <a href=\"#\"> {{thumbnail}} </a>\n    <p>Exercitation tempor in nulla et cupidatat laboris do id excepteur ullamco.</p>\n  </li>\n\n  <li>\n    <a href=\"#\"> {{thumbnail}} </a>\n    <p>Adipisicing laborum ad. In ullamco fugiat magna elit. Voluptate eiusmod eiusmod.</p>\n  </li>\n</ul>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s=",
        "preSelector": "$('.gallery a')"
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 9,
      "category": "traversing",
      "instructions": "<h2>Traverse to the unordered list that is a child of <code>li.current</code></h2>\n",
      "htmlStart": "<ol>\n  <li class=\"current\">\n    <a href=\"#\">Section 1</a>\n\n    <ul data-correct=\"true\"> <!-- Just this <ul> yo. -->\n\n      <li><a href=\"#\">Section 1a</a></li>\n      <li>\n        <a href=\"#\">Section 1b</a>\n\n        <ul>\n          <li><a href=\"#\">Section 1b1</a></li>\n        </ul>\n\n      </li>\n\n      <li><a href=\"#\">Section 1c</a></li>\n      <li><a href=\"#\">Section 1d</a></li>\n      <li><a href=\"#\">Section 1e</a></li>\n\n    </ul> <!-- Just this <ul> yo. -->\n\n  </li>\n</ol>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s=",
        "preSelector": "$('li.current')"
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 10,
      "category": "traversing",
      "instructions": "<h2>Traverse to both unordered lists</h2>\n",
      "htmlStart": "<ol>\n  <li class=\"current\">\n    <a href=\"#\">Section 1</a>\n\n    <ul data-correct=\"true\">\n\n      <li><a href=\"#\">Section 1a</a></li>\n      <li>\n        <a href=\"#\">Section 1b</a>\n\n        <ul data-correct=\"true\">\n          <li><a href=\"#\">Section 1b1</a></li>\n        </ul>\n\n      </li>\n\n      <li><a href=\"#\">Section 1c</a></li>\n      <li><a href=\"#\">Section 1d</a></li>\n      <li><a href=\"#\">Section 1e</a></li>\n\n    </ul>\n\n  </li>\n</ol>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s=",
        "preSelector": "$('li.current')"
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    },
    {
      "id": 11,
      "category": "traversing",
      "instructions": "<h2>Traverse to the anchor elements</h2>\n",
      "htmlStart": "<ul class=\"kitty-list\">\n  <li>\n    <a data-correct=\"true\" href=\"kitties.php?id=1\"> {{thumbnail}} </a>\n  </li>\n\n  <li>\n    <a data-correct=\"true\" href=\"kitties.php?id=2\"> {{thumbnail}} </a>\n  </li>\n\n  <li>\n    <a data-correct=\"true\" href=\"kitties.php?id=3\"> {{thumbnail}} </a>\n  </li>\n\n  <li>\n    <a data-correct=\"true\" href=\"kitties.php?id=4\"> {{thumbnail}} </a>\n  </li>\n</ul>\n",
      "commands": {
        "finalAssert": "(function(window, $) {\n    var expected = $('[data-correct=\"true\"]').get();\n    var answer = window.$s.get();\n    window.isCorrect = (function(a, b) {\n        var l = a.length;\n        if (l !== b.length) return false;\n        while (l--) {\n            if (a[l] !== b[l]) return false;\n        }\n        return true;\n    }($.unique(answer), $.unique(expected)));\n}(window, jQuery));",
        "firstAssert": "(function($s, $) {\n    if (!($s instanceof $)) {\n        throw new Error('Input is not a jQuery object');\n    }\n}(window.$s, jQuery));",
        "post": "$s.attr('s', function() {\n    return $(this).data('correct') ? '✓' : 'x';\n});",
        "pre": "$('[s]').removeAttr('s');",
        "prefix": "window.$s=",
        "preSelector": "$('.kitty-list')"
      },
      "userData": {
        "answer": null,
        "html": null,
        "timestamp": 0
      }
    }
  ]
};
