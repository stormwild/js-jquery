$(function() {

    var nav = $('.main-nav')
    var win = $(window)
    var navTop = nav.offset().top

    function toggleStickyNav() {
        if (win.scrollTop() > navTop) {
            nav.addClass('sticky')
        } else {
            nav.removeClass('sticky')
        }
    }

    win.on('scroll', toggleStickyNav)
    toggleStickyNav()


    var jumpLinks = nav.find('a')
    var scrollables = $('html, body')
    var navHeight = nav.height()

    function doScroll(event) {
        var id = $(this).attr('href')
        var top = $(id).offset().top

        event.preventDefault()

        scrollables.animate({
            scrollTop: top - navHeight
        })
    }

    jumpLinks.on('click', doScroll)

});
