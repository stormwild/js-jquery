var Problem = function(x, y){
  this.x = x;
  this.y = y; 
}

var problem1 = new Problem(4, 5);
var problem2 = new Problem(5, 5);

Problem.prototype.operations = {
  '+': function(x,y){ return x + y; },
  '-': function(x,y){ return x - y; }
};

Problem.prototype.calculate = function(operation){
  return this.operations[operation](this.x, this.y);
};

console.log( problem1.calculate('-') );
console.log( problem2.calculate('+') );

// Closure
Problem.prototype.newMessageMaker = function(){
    var self = this;
    var formatter = function(){
        return 'Values: ' + self.x + ' and ' + self.y;
    };

  return function(start, end){
    return '' + start + formatter() + end;
  };
}

var messageMaker = problem1.newMessageMaker();

console.log( messageMaker('"', '"') );


