# Apply Javascript

## Objectives

- To apply JavaScript inline
- To apply JavaScript embedded within a page 
- To link to an external JavaScript file

## Exercise

- Add an inline script to the body tag.
- Add an inline script to a link.
- Add an embedded script tag to the head.
- Add an embedded script tag to the body.
- Add an external JavaScript file

## Important Concepts

JavaScript blocks browser page rendering.

JavaScript executes as it is encountered within the HTML page.

Use the browser console to display information. 

The console is useful for debugging.

JavaScript is executed in response to events to which they are assigned such as onload and onclick.


