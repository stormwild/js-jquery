# Intro to the DOM

## Learning Objectives

## Learning Methodology

### Discuss the DOM

1. The Document Object Model (DOM) is an application programming interface (API) for XML that was extended for use in HTML. The DOM maps out an entire page as a hierarchy of nodes. Each part of an HTML or XML page is a type of a node containing different kinds of data. Consider the following HTML page:

<html>
<head>
    <title>Sample Page</title>
</head>
<body>
    <p>Hello World!</p>
</body>
</html>

This code can be diagrammed into a hierarchy of nodes using the DOM (see Figure 1-2).

By creating a tree to represent a document, the DOM allows developers an unprecedented level of control over its content and structure. Nodes can be removed, added, replaced, and modified easily by using the DOM API.


## Learning Evaluation
