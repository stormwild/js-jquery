var par = document.getElementsByTagName('p');
console.log(par);

var starList = document.getElementById('stars');

var stars = document.getElementsByClassName('star');

console.log(stars);

var newStar = document.createElement('li');
newStar.textContent = 'Orion';
newStar.setAttribute('class', 'star');

starList.appendChild(newStar);

var len = stars.length;
for(var i = 0; i < len; i++) {
    stars[i].addEventListener('click', function(){
        this.classList += ' highlight';
    })
}