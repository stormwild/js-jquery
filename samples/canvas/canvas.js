(function ($) {

    var canvas = $('.canvas')[0]; // html node/element

    canvas.width = 400;
    canvas.height = 300; // the canvas is now 400x300 pixels

    console.log(canvas.width);

    function draw() {
        var ctx = canvas.getContext("2d"); // create a 2D context object

        ctx.beginPath();
        ctx.moveTo(125, 50);
        ctx.lineTo(200, 200);
        ctx.lineTo(50, 125);
        ctx.closePath();

        ctx.fillStyle = "rgba(255,150,50,0.5)";
        ctx.fill();

        ctx.strokeStyle = "red";
        ctx.lineWidth = 2.0;
        ctx.stroke();
        
        ctx.fillStyle = "rgba(255,150,50,0.5)";
        ctx.fillRect(150, 200, 100, 30);

        ctx.strokeStyle = "red";
        ctx.strokeRect(150, 200, 100, 30);
    }

    function clear() {
        canvas.width = canvas.width; // canvas is now cleared
    }

    var $drawBtn = $('.draw-btn'),
        $clearBtn = $('.clear-btn');

    $drawBtn.on('click', draw);
    $clearBtn.on('click', clear);

})(jQuery);