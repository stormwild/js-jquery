(function ($) {

    var $form = $('#register'),
        errors = [],
        
        validate = function() {
            var $field = $(this),
                val = $.trim( $field.val() );
            
            if($field.data('required') === true && val === '') {    
                errors.push($field);
                $field.addClass('invalid');
            }
            
            if( $field.data('email') === true && !/\S+@\S+\.\S+/g.test(val) ) {
                errors.push($field);
                $field.addClass('invalid');
            }
        },
        
        submit = function (e) {            
            var $fields = $form.find('[name]'),
                $errorMsg = $('<ul/>').addClass('errors');
            
            errors = [];            
            $fields.removeClass('invalid');            
            $fields.each(validate);
            
            $.each(errors, function(i, $field){
                var msg =  $field.data('error-msg') || $field.attr('name') + ' is required',
                    $error = $('<li/>');
                $errorMsg.append( $error.html( msg ) );
            });
            
            if($form.find('.errors').length > 0) {
                $form.find('.errors').replaceWith($errorMsg);
            } else {
                $form.prepend($errorMsg);
            }
            
            if(errors.length === 0) {
                return true;
            }
            return false;
        };

    $form.on('submit', submit);

})(jQuery);