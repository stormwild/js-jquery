# Untitled

## .css() phase 0
1. Create a function named styleSingleBox.
2. styleSingleBox should select the single target div and change/add css
   styles using jQuery's .css() method.

## .css() phase 1
1. Create a function named styleMultipleBoxes.
2. styleMultipleBoxes should select the three target divs and change/add css
   styles using jQuery's .css() method.

## .addClass()
1. Open untitled.css and add a new style rule using a class name(s) of your
   choice as the selector.
2. Create a function named addClasses
3. addClasses should select the four target divs and add the class(es) you
   created.

## .removeClass()
1. Create a function named removeClasses.
2. removeClasses should select the target divs with a red overlay and remove
   their "remove-this" class.

## .toggleClass()
1. Create a function named toggleClasses.
2. toggleClasses should select the four target divs and toggle the class(es) you
   created.
