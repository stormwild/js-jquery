(function($) {

    $.mockjax(function(settings) {
        var re = /(kitty\d)\.(json|html)/g;
        var matches = re.exec(settings.url);
        var kitty = matches[1];

        if (parseInt(kitty, 10) > 4) { return; }

        var type = matches[2];
        var contentType = (type === 'json' ? 'application' : 'text') + '/' + type;
        var file = 'content/' + kitty + '.' + type;

        return {
            responseTime: 400,
            status: 200,
            contentType: contentType,
            proxy: file
        }
    });

}(jQuery));
