$(function() {

    var gridItems = $('.grid li')
    var maxHeight = 0

    function findTallest() {
        var height = $(this).outerHeight()
        if (height > maxHeight) {
            maxHeight = height
        }
    }

    gridItems
        .each(findTallest)
        .height(maxHeight)

})
