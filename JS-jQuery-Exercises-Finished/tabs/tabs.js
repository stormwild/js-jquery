$(function() {

    var tabs = $('.tabs-nav a')
    var panels = $('.tabs-content article')

    function showPanel(event) {
        var tab = $(this)
        var index = tab.index()
        var panel = panels.eq(index)

        event.preventDefault()

        tabs.removeClass('current')
        tab.addClass('current')

        panels.removeClass('current')
        panel.addClass('current')
    }

    tabs.on('click', showPanel)

})
