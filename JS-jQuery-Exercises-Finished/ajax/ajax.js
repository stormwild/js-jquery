$(function() {

    var kitties = $('.kitty-list a')
    var content = $('.kitty-content')
    var doc = $(document)

    function getContent(event) {
        var kitty = $(this)
        var href = kitty.attr('href')
        event.preventDefault()
        $.ajax({
            url: href + '.html',
            success: function(data) {
                content.html(data)
            }
        })
    }

    function toggleLoader() {
        content.toggleClass('loading')
    }


    kitties.on('click', getContent)
    doc.on('ajaxStart ajaxComplete', toggleLoader)

})
