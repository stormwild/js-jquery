var jQLabsData = jQLabsData || {};
jQLabsData["categories"] = [
  {
    "id": "selecting",
    "title": "Selecting Elements",
    "description": "<p>Sed tincidunt diam id sem commodo auctor. Mauris molestie venenatis enim et\ngravida. Vestibulum malesuada arcu eget elit facilisis viverra. Ut facilisis\ncondimentum blandit.</p>\n<ul>\n<li>Suspendisse potenti.</li>\n<li>Aliquam erat volutpat.</li>\n<li>Vivamus congue dictum nunc</li>\n</ul>\n<p>In mattis libero molestie id. Pellentesque sit amet dolor hendrerit, condimentum\nodio eu, pellentesque turpis.</p>\n<p>Quisque venenatis euismod egestas. Fusce auctor tortor eu tempus consequat. Ut\ndapibus imperdiet risus, quis laoreet nunc rhoncus sed. Aliquam volutpat est\nturpis, quis ultrices arcu vestibulum vitae. Aenean blandit euismod nisl, quis\nblandit mauris suscipit nec. Curabitur a leo eget dui rhoncus condimentum. Nunc\naliquet posuere neque a imperdiet. Nullam lorem orci, faucibus nec elit in,\nscelerisque fringilla erat. Fusce accumsan velit nec blandit volutpat. Sed non\nluctus nibh.</p>\n"
  },
  {
    "id": "traversing",
    "title": "Traversing to Other Elements",
    "description": "<p>Curabitur a leo eget dui rhoncus condimentum. Nunc aliquet posuere neque a\nimperdiet. Nullam lorem orci, faucibus nec elit in, scelerisque fringilla\nerat. Aliquam erat volutpat. Vivamus congue dictum nunc, in mattis libero\nmolestie id. Pellentesque sit amet dolor hendrerit, condimentum odio eu,\npellentesque turpis.</p>\n<p>Quisque venenatis euismod egestas. Fusce auctor tortor eu tempus consequat. Ut\ndapibus imperdiet risus, quis laoreet nunc rhoncus sed. Aliquam volutpat est\nturpis, quis ultrices arcu vestibulum vitae. Aenean blandit euismod nisl, quis\nblandit mauris suscipit nec.</p>\n<p>Fusce accumsan velit nec blandit volutpat. Sed non luctus nibh. Sed tincidunt\ndiam id sem commodo auctor. Mauris molestie venenatis enim et\ngravida. Vestibulum malesuada arcu eget elit facilisis viverra. Ut facilisis\ncondimentum blandit. Suspendisse potenti.</p>\n"
  },
  {
    "id": "filtering",
    "title": "Filtering the Collection",
    "description": "<p>Quisque venenatis euismod egestas. Fusce auctor tortor eu tempus consequat. Ut\ndapibus imperdiet risus, quis laoreet nunc rhoncus sed. Aliquam volutpat est\nturpis, quis ultrices arcu vestibulum vitae. Aenean blandit euismod nisl, quis\nblandit mauris suscipit nec. Curabitur a leo eget dui rhoncus condimentum. Nunc\naliquet posuere neque a imperdiet. Nullam lorem orci, faucibus nec elit in,\nscelerisque fringilla erat. Fusce accumsan velit nec blandit volutpat. Sed non\nluctus nibh.</p>\n<p>Sed tincidunt diam id sem commodo auctor. Mauris molestie venenatis enim et\ngravida. Vestibulum malesuada arcu eget elit facilisis viverra. Ut facilisis\ncondimentum blandit. Suspendisse potenti. Aliquam erat volutpat. Vivamus congue\ndictum nunc, in mattis libero molestie id. Pellentesque sit amet dolor\nhendrerit, condimentum odio eu, pellentesque turpis.</p>\n"
  }
];
