# Gallery Side

## Phase 0
1.  Select the element with the class of "img-viewer". Store it in a variable
    (imgViewer).
2.  Select imgViewer's descendant image element. Store it in a variable (bigImg).
3.  Select all anchor elements that are descendants of ".thumbnails".
4.  Declare a function named showImage. Keep this function empty for now.
5.  Attach a click event handler to thumbnails, passing in the showImage function
    from #4.
5.  Add functionality to the showImage function.
    a. Get bigImg's src attribute value. Store it in a variable (currentImgSrc).
    b. Get the clicked anchor's href attribute value. Store it in a variable
       (newImgSrc).
    c. Prevent the click event's default behavior.
    d. Using an if statement, check if currentImgSrc is equal to newImgSrc. If it
       is, return out of the function.
    e. Set bigImg's src attribute value to newImgSrc.


## Phase 1
6.  Declare a function named hideLoader. Keep this function empty for now.
7.  Attach a load event handler to bigImg, passing in the hideLoader function
    from #6.
9.  Add more functionality to the showImage function.
    a. Add a class of "loading" to imgViewer.
10. Add functionality to the hideLoader function.
    a. Remove the class of "loading" from imgViewer.
