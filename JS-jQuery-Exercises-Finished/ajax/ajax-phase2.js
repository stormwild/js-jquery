$(function() {

    var kitties = $('.kitty-list a')
    var content = $('.kitty-content')
    var doc = $(document)

    function getContent(event) {
        var kitty = $(this)
        var href = kitty.attr('href')
        event.preventDefault()
        $.ajax({
            url: href + '.json',
            success: function(data) {
                var html = '<article>'
                html = html + '<img src="' + data.img + '">'
                html = html + '<h1>' + data.name + '</h1>'
                html = html + '<div><p>' + data.description + '</p></div>'
                html = html + '</article>'
                content.html(html)
            }
        })
    }

    function toggleLoader() {
        content.toggleClass('loading')
    }


    kitties.on('click', getContent)
    doc.on('ajaxStart ajaxComplete', toggleLoader)

})
