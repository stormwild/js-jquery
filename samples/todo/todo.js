(function($){
    
    var $todo = $('input[name=todo]'), 
        $form = $('#form-todo'),
        $todos = $('.todos'), // ul
        $totalTodo = $('.total-todo'),
        $removeLinks = $todos.find('.remove-todo'),
        $deleteAllTodoBtn = $('.delete-all-todo'),
        todoList = [];    
    
    function addTodo(e) {
        e.preventDefault();        
        todoList.unshift( $todo.val() );              
        $todos.prepend('<li>' + $todo.val() + ' <a href="#" class="remove-todo">x</a></li>');
        $totalTodo.html('(' + todoList.length + ')');
    }
    
    function removeTodo(e) {
        e.preventDefault();     
        
        var index = $(this).parent().index(); 
        todoList.splice(index, 1);
        $todos.find('li').eq(index).remove();
        $totalTodo.html('(' + todoList.length + ')');
    }
    
    function removeAllTodos(e) {
        e.preventDefault(); 
        
        var confirmed = confirm('Are you sure?');
        if(confirmed) {
            todoList = [];
            $totalTodo.html('(' + todoList.length + ')');
            $todos.html('');        
        }
    }
    
    $form.on('submit', addTodo);
    $todos.on('click', '.remove-todo', removeTodo);
    $deleteAllTodoBtn.on('click', removeAllTodos);
    
})(jQuery);